import 'package:flutter/material.dart';
import 'package:pcs_lens/qr_scanner.dart';
import 'package:pcs_lens/pc_list.dart';

// Import package to manage splashscreen
import 'package:splashscreen/splashscreen.dart';

void main(){
  runApp(new MaterialApp(
      title: 'Pc`s Lens',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.cyan,
      ),
      home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
        seconds: 3,
        navigateAfterSeconds: new MyHomePage(title: 'Pc`s lens '),
        title: new Text('Bienvenido a Pc`s Lens',
          style: new TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20.0,
              color: Colors.cyan
          ),),
        image: new Image.asset('assets/images/logo.jpeg'),
        backgroundColor: Colors.white,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 100.0,
        loaderColor: Colors.cyanAccent
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title, style: TextStyle(color: Colors.white)),
        backgroundColor: Colors.cyan,
        centerTitle: true,
      ),
      body: Center(
        child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            //crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(60, 10, 60, 10),
                child: FittedBox(
                  fit:BoxFit.cover,
                  alignment: Alignment.center,
                  child: Image.asset('assets/images/logo.jpeg'),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                child: const Text('Bienvenido a Pc`s Lens', style: TextStyle(color: Colors.cyan, fontSize: 16, fontWeight: FontWeight.w800),)
              ),
              SizedBox(
                width: 200,
                child: RaisedButton(
                    textColor: Colors.white,
                    color: Colors.cyan,
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) =>  PcList()),
                      );
                    },
                    child: const Text('Mostrar lista')
                ),
              ),
            ]
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ScanScreen()),
          );
        },
        backgroundColor: Colors.cyan,
        foregroundColor: Colors.white,
        tooltip: 'Abrir Cámara',
        child: Icon(Icons.camera_alt),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
