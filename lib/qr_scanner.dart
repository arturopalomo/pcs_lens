import 'dart:async';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pcs_lens/pc_list.dart';
import 'package:pcs_lens/storage.dart';

class ScanScreen extends StatefulWidget {
  @override
  _ScanState createState() => new _ScanState();
}

class _ScanState extends State<ScanScreen> {
  String barcode = "";
  List<Computer> _pcListItems;
  FakeStorage serviceC = new FakeStorage();

  @override
  initState() {
    super.initState();
    _pcListItems = new List<Computer>();
    _pcListItems = serviceC.fetch();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          title: new Text('Escánear QR', style: TextStyle(color: Colors.white),),
        ),
        body: new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                child: RaisedButton(
                    color: Colors.cyan,
                    textColor: Colors.white,
                    splashColor: Colors.cyanAccent,
                    onPressed: scan,
                    child: const Text('Activar Cámara')
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                child: Text(barcode, textAlign: TextAlign.center,),
              )
              ,
            ],
          ),
        ));
  }

  Future scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      for (int i = 0; i < _pcListItems.length; i++) {
        var item = _pcListItems[i];
        print(item);
        if (item.model.toLowerCase().contains(barcode.toLowerCase())) {
          print('Si encontró ${barcode}');
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) =>
                DetailComputer(item)),
          );
        }
      }
      print(barcode);
      setState(() => {
        this.barcode = barcode
      });

    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'El usuario no dió permisos a la cámara!';
        });
      } else {
        setState(() => this.barcode = 'Código desconocido: $e');
      }
    } on FormatException{
      setState(() => this.barcode = 'No se encontraron resultados.');
    } catch (e) {
      setState(() => this.barcode = 'Código desconocido: $e');
    }
  }
}