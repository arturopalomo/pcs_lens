class Computer {
  String id;
  String brand;
  String model;
  String thumbnail;
  String processor;
  String ram;
  String hdd;
  String gpu;
  String os;
  String displaySize;
  String color;
  String type;


  Computer(this.id, this.brand, this.model, this.thumbnail, this.processor,
      this.ram, this.hdd, this.gpu, this.os, this.displaySize, this.color,
      this.type);
}

abstract class StorageService {
  List<Computer> fetch();
}

class FakeStorage implements StorageService {
  @override
  List<Computer> fetch() {
    return <Computer> [
      new Computer(
          'pc0',
          'Lenovo',
          '80XS',
          'assets/images/lenovo80xs.jpeg',
          'AMD A10',
          '8 GB',
          '1 TB',
          'AMD Radeon R7',
          'Windows 10',
          '15 inch 1366x768p',
          'Plata',
          'laptop'),
      new Computer(
          'pc1',
          'Asus',
          'E203MA',
          'assets/images/asuse203ma.jpg',
          'Intel Celeron N4000',
          '2 GB',
          '32 GB SSD',
          'Intel UHD Graphics 600',
          'Windows 10',
          '13 inch HD',
          'Plata',
          'laptop'),
      new Computer(
          'pc2',
          'Acer',
          'C720-2103',
          'assets/images/acerc720.jpeg',
          'Intel Celeron 2955U',
          '2 GB',
          '32 GB SSD',
          'Intel HD Graphics',
          'Chrome OS',
          '11 inch',
          'Roja',
          'laptop'),
      new Computer(
          'pc3',
          'Toshiba',
          'Satellite C50D-A',
          'assets/images/toshibac50d.jpeg',
          'AMD E1',
          '4 GB',
          '1 TB',
          'AMD Graphics',
          'Windows 10',
          '15 inch',
          'Blanca',
          'laptop'),
      new Computer(
          'pc4',
          'HP',
          '6305',
          'assets/images/hp6305.jpg',
          'AMD A8',
          '8 GB',
          '1 TB',
          'AMD Graphics',
          'Windows 8',
          '20 inch',
          'Negra',
          'desktop'),
      new Computer(
          'pc5',
          'Apple',
          'Macbook Air',
          'assets/images/macbookair.jpeg',
          'Intel Core i5 6a Gen',
          '8 Gb DDR4',
          '256 SSD',
          '2 GB',
          'Windows 10',
          '13 inch',
          'Silver Space',
          'laptop'),
      new Computer(
          'pc6',
          'HP',
          'BoomSound',
          'assets/images/hproja.jpeg',
          'AMD A10 6a Gen',
          '16 Gb DDR4',
          '1 TB',
          '2 GB',
          'Windows 10',
          '15.3 inch',
          'Red Passion',
          'laptop'),
    ];
  }

}