import 'package:flutter/material.dart';
import 'package:pcs_lens/storage.dart';

class PcList extends StatefulWidget {
  @override
  _PcListScreenState createState() => _PcListScreenState();
}

class _PcListScreenState extends State<PcList> {
  List<Computer> _pcListItems;
  FakeStorage serviceC = new FakeStorage();

  @override
  void initState() {
    super.initState();
    _pcListItems = new List<Computer>();
    _pcListItems = serviceC.fetch();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text(
          'Computadoras Disponibles', style: TextStyle(color: Colors.white),),
      ),
      body:
      Container(
        margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
        child: Column(
          children: <Widget>[
            _listView(),
          ],
        ),
      ),
    );
  }

  Widget _listView() {
    return Flexible(
      child: ListView.builder(
        itemCount: _pcListItems.length,
        itemBuilder: (context, index) {
          return Padding(
              padding: const EdgeInsets.all(16.0),
              child: GestureDetector(
                onTap: () {
                  print("Container clicked");
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) =>
                        DetailComputer(_pcListItems[index])),
                  );
                },
                child: Container(
                  child: FittedBox(
                    child: Material(
                      color: Colors.white,
                      elevation: 40.0,
                      borderRadius: BorderRadius.circular(4.0),
                      shadowColor: Color(0x002196F3),
                      child: Row(
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Container(
                                child: Text("${_pcListItems[index].brand}",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(color: Colors.black54,
                                        fontSize: 6,
                                        fontWeight: FontWeight.w800)),
                              ),
                              Container(
                                child: Text("${_pcListItems[index].model}",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(color: Colors.black45,
                                        fontSize: 4,
                                        fontWeight: FontWeight.w300)),
                              ),
                            ],
                          ),
                          FittedBox(
                            fit: BoxFit.contain,
                            child: Container(
                              width: 30,
                              height: 30,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(4.0),
                                child: Image(
                                    fit: BoxFit.contain,
                                    alignment: Alignment.topRight,
                                    image: AssetImage(
                                        '${_pcListItems[index].thumbnail}'
                                    )
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )
          );
        },
      ),
    );
  }
}

class DetailComputer extends StatelessWidget {
  Computer _pcDetail;

  DetailComputer(this._pcDetail);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                expandedHeight: 300.0,
                floating: false,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                    centerTitle: true,
                    title: Text("${_pcDetail.brand} ${_pcDetail.model}",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 19.0,
                        )
                    ),
                    background: Image.asset(
                      '${_pcDetail.thumbnail}',
                      fit: BoxFit.cover,
                    )),
              ),
            ];
          },
          body: Column(
            children: <Widget>[
              Row(
                  children: <Widget>[
                    new Center(
                        child: new Container(
                            child: new Text('${_pcDetail.brand} ${_pcDetail.model}',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.black54,
                                  fontFamily: 'Roboto',
                                  fontSize: 26,
                                  fontWeight: FontWeight.w800),
                            )
                        )
                    ),
                  ]
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                child: Row(
                    children: <Widget>[
                      Center(
                          child: new Text('Características:',
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.black54,
                                fontFamily: 'Roboto',
                                fontSize: 26,
                                fontWeight: FontWeight.w600),
                          )
                      )
                    ]
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                child: Table(children: [
                  TableRow(children: [
                    Text("Modelo", textAlign: TextAlign.center, style: TextStyle(
                        color: Colors.black87, fontSize: 20)),
                    Text("${_pcDetail.model}", textAlign: TextAlign.center, style: TextStyle(
                        color: Colors.black87, fontSize: 20)),
                      ]
                  ),
                  TableRow(children: [
                    Text("Sistema Operativo", textAlign: TextAlign.center, style: TextStyle(
                        color: Colors.black87, fontSize: 20)),
                    Text("${_pcDetail.os}", textAlign: TextAlign.center, style: TextStyle(
                        color: Colors.black87, fontSize: 20)),
                  ]),
                  TableRow(children: [
                    Text("Procesador", textAlign: TextAlign.center, style: TextStyle(
                        color: Colors.black87, fontSize: 20)),
                    Text("${_pcDetail.processor}", textAlign: TextAlign.center, style: TextStyle(
                        color: Colors.black87, fontSize: 20)),
                  ]),
                  TableRow(children: [
                    Text("Memoria Ram", textAlign: TextAlign.center, style: TextStyle(
                        color: Colors.black87, fontSize: 20)),
                    Text("${_pcDetail.ram}", textAlign: TextAlign.center, style: TextStyle(
                        color: Colors.black87, fontSize: 20)),
                  ]),
                  TableRow(children: [
                    Text("Disco Duro", textAlign: TextAlign.center, style: TextStyle(
                        color: Colors.black87, fontSize: 20)),
                    Text("${_pcDetail.hdd}", textAlign: TextAlign.center, style: TextStyle(
                        color: Colors.black87, fontSize: 20)),
                  ]),
                  TableRow(children: [
                    Text("Gráficos", textAlign: TextAlign.center, style: TextStyle(
                        color: Colors.black87, fontSize: 20)),
                    Text("${_pcDetail.gpu}", textAlign: TextAlign.center, style: TextStyle(
                        color: Colors.black87, fontSize: 20)),
                  ]),
                  TableRow(children: [
                    Text("Color", textAlign: TextAlign.center, style: TextStyle(
                        color: Colors.black87, fontSize: 20)),
                    Text("${_pcDetail.color}", textAlign: TextAlign.center, style: TextStyle(
                        color: Colors.black87, fontSize: 20)),
                  ]),
                  TableRow(children: [
                    Text("Display", textAlign: TextAlign.center, style: TextStyle(
                        color: Colors.black87, fontSize: 20)),
                    Text("${_pcDetail.displaySize}", textAlign: TextAlign.center, style: TextStyle(
                        color: Colors.black87, fontSize: 20)),
                  ]),
                ]),
              ),
            ],
          )
      ),
    );
  }
}
